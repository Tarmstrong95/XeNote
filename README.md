## What is XeNote?
XeNote is a free open-source alternative solution to [Obsidian Publish](https://obsidian.md/publish)

Here's how it looks once published, checkout demo here: [demo](https://tristonarmstrong.com/notes)

This website includes a published version of default Obsidian Help vault

**XeNote features:**

-  ✅ **Drop-in** support for (default) **Obsidian Vault**
-  ✅ `[[Wiki Link]]` built-in support
-  ✅ **Folder-base** navigation side bar
-  ✅ Backlink support out of the box
-  ✅ **Easy to deploy** to Netlify, Vercel...

## Getting started
### Run on your local machine

Steps to run it on your local machine:
1. Clone this [Github repo](https://gitlab.com/Tarmstrong95/XeNote.git)
2. Install [bun](https://bun.sh) 
3. Copy all of your images from your Obsidian Vault to `/public/images/` folder
4. Go to root folder of your project, run `bund dev`
5. Open this link in your browser http://localhost:3000/


Your normal workflow for publishing content, after initial setup is:
1. Simply writing your content in Obisidian (or your favourite Markdown editor)
2. Commit your changes and Push it to your Github repo

