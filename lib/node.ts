import path from "path";
import fs from "fs";
import type { FilePath } from "./types";

class Node {
  _markdownFolder: string | undefined;
  _filesList: string[];

  constructor() {
    this._markdownFolder = undefined;
    this._filesList = [];
  }

  isFile = (filename: string): boolean => {
    try {
      return fs.existsSync(filename);
    } catch (err) {
      console.error(err);
      return false;
    }
  };

  getFullPath = (folderPath: string): FilePath[] => {
    return fs.readdirSync(folderPath).map((fn) => path.join(folderPath, fn));
  };

  getFiles = (dir: string): FilePath[] => {
    var results: FilePath[] = [];
    var list = fs.readdirSync(dir);

    list.forEach((file) => {
      file = dir + "/" + file;
      var stat = fs.statSync(file);

      if (!stat || !stat.isDirectory()) {
        return results.push(file);
      }

      const subFiles = this.getFiles(file);
      results = results.concat(subFiles);
    });

    const filtered = results.filter((f) => f.endsWith(".md"));

    this._filesList = filtered;
    return this._filesList;
  };

  readFileSync = (fullPath: FilePath) => {
    return fs.readFileSync(fullPath, "utf8");
  };

  getMarkdownFolder = (): string => {
    if (this._markdownFolder) return this._markdownFolder;
    const notesDir = path.join(process.cwd(), "notes");
    if (!this.isFile(notesDir)) {
      console.warn("Notes Directory does not seem to exist: ", notesDir);
    }
    this._markdownFolder = notesDir;
    return this._markdownFolder;
  };
}

const node = new Node();
export default node;
