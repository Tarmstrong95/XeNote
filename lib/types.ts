/** The base route object that the navigation pane utilizes to route the user
 *
 * */
export interface RouteObj {
  name: string;
  children: RouteObj[];
  id: string;
  routePath: string | null;
}

/** A slug representation of a FilePath type
 * @example
 * ``` typescript
 * filePath = '/some/file/path/to/a/document.md'
 * slug = '_some_file_path_to_a_document'
 * ```
 * */
export type Slug = string;

/** Obviously a file path
 * @example
 * ``` typescript
 * filePath = '/some/file/path/to/a/document.md'
 * ```
 * */
export type FilePath = string;

/** A backlink is a link that points back to a file that references the current file */
export interface BackLink {
  title: string | null;
  slug: string;
  shortSummary: string;
}
