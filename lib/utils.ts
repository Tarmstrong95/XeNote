import { default as Node } from "./node";
import { Transformer } from "./transformer";
import { unified } from "unified";
import markdown from "remark-parse";
import { toString } from "mdast-util-to-string";
import path from "path";
import fs from "fs";
import type { FilePath, RouteObj, Slug } from "./types";

const dirTree = require("directory-tree");

class Util {
  _counter;
  _cachedSlugMap: Map<Slug, FilePath>;
  _directoryData: RouteObj | null;

  constructor() {
    this._counter = 0;
    this._cachedSlugMap = this.getSlugHashMap();
    this._directoryData = null;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  getContent(slug: Slug): string | null {
    const currentFilePath = this.toFilePath(slug);
    if (currentFilePath === undefined || currentFilePath == null) return null;
    return Node.readFileSync(currentFilePath);
  }

  getShortSummary(slug: Slug) {
    const content = this.getContent(slug);
    if (content === undefined || content === null) {
      return;
    }

    const tree = unified().use(markdown).parse(content);
    const plainText = toString(tree);
    return plainText.split(" ").splice(0, 40).join(" ");
  }

  /** gets all the `.md` files in the notes directory */
  getAllMarkdownFiles(): FilePath[] {
    const markdownFolder = Node.getMarkdownFolder();
    const files = Node.getFiles(markdownFolder);
    return files;
  }

  getSinglePost(slug: Slug) {
    // List of filenames that will provide existing links to wikilink
    const currentFilePath = this.toFilePath(slug);

    const fileContent = Node.readFileSync(currentFilePath);

    // const currentFileFrontMatter = Transformer.getFrontMatterData(fileContent)
    const [htmlContent] = Transformer.getHtmlContent(fileContent);
    return {
      id: slug,
      // ...currentFileFrontMatter,
      data: htmlContent,
    };
  }

  toFilePath(slug: Slug): FilePath | undefined {
    const filePath = this._cachedSlugMap.get(slug);
    return filePath;
  }

  getSlugHashMap(): Map<Slug, FilePath> {
    // This is to solve problem of converting between slug and filepath,
    // where previously if I convert a slug to a file path sometime
    // it does not always resolve to correct filepath, converting function is not bi-directional
    // and not conflict-free, other solution was considered (hash file name into a hash, but this
    // is not SEO-friendly and make url look ugly ==> I chose this

    const slugMap = new Map<Slug, FilePath>();
    this.getAllMarkdownFiles().forEach((aFile: FilePath) => {
      const aSlug: Slug = this.toSlug(aFile);
      slugMap.set(aSlug, aFile);
    });

    // hard defined root file
    const indexFile = "/Home.md";
    slugMap.set("index", Node.getMarkdownFolder() + indexFile);
    slugMap.set("/", Node.getMarkdownFolder() + indexFile);

    return slugMap;
  }

  /** takes a path `some/path/file` and converts it to `some_path_file` to generate a slug*/
  toSlug(filePath: FilePath): Slug {
    const markdownFolder = Node.getMarkdownFolder();
    const isFile = Node.isFile(filePath);
    const isMarkdownFolder = filePath.includes(markdownFolder);

    if (isFile && isMarkdownFolder) {
      const newPath = filePath
        .replace(markdownFolder, "")
        .replaceAll("/", "_")
        .replaceAll(" ", "+")
        .replaceAll("&", "-")
        .replace(".md", "");
      return newPath;
    }

    // TODO handle this properly
    return "/";
  }

  constructGraphData() {
    const filepath = path.join(process.cwd(), "graph-data.json");
    if (Node.isFile(filepath)) {
      const data = fs.readFileSync(filepath);
      return JSON.parse(String(data));
    }
    const filePaths = this.getAllMarkdownFiles();
    const edges: Array<{ source: string; target: string }> = [];
    const nodes: Array<{ title: string; slug: string; shortSummary: string }> =
      [];

    filePaths.forEach((aFilePath) => {
      const aNode = {
        title: Transformer.parseFileNameFromPath(aFilePath),
        slug: this.toSlug(aFilePath),
        shortSummary: this.getShortSummary(this.toSlug(aFilePath)),
      };
      nodes.push(aNode as (typeof nodes)[number]);

      const internalLinks = Transformer.getInternalLinks(aFilePath);
      internalLinks.forEach((aLink) => {
        if (aLink.slug === null || aLink.slug.length === 0) return;

        const anEdge = {
          source: this.toSlug(aFilePath),
          target: aLink.slug,
        };
        edges.push(anEdge);
      });
    });
    const data = { nodes, edges };
    fs.writeFileSync(filepath, JSON.stringify(data), "utf-8");
    return data;
  }

  getLocalGraphData(currentNodeId: string) {
    const { nodes, edges } = this.constructGraphData();

    const newNodes = nodes.map((aNode: any) => ({
      data: {
        id: aNode.slug.toString(),
        label: Transformer.parseFileNameFromPath(this.toFilePath(aNode.slug)),
      },
    }));

    const newEdges = edges.map((anEdge: any) => ({
      data: {
        source: anEdge.source,
        target: anEdge.target,
      },
    }));

    const existingNodeIDs = newNodes.map((aNode: any) => aNode.data.id);
    currentNodeId = currentNodeId === "index" ? "__index" : currentNodeId;
    if (
      currentNodeId != null &&
      Boolean(existingNodeIDs.includes(currentNodeId))
    ) {
      const outGoingNodeIds = newEdges
        .filter((anEdge: any) => anEdge.data.source === currentNodeId)
        .map((anEdge: any) => anEdge.data.target);

      const incomingNodeIds = newEdges
        .filter((anEdge: any) => anEdge.data.target === currentNodeId)
        .map((anEdge: any) => anEdge.data.source);

      outGoingNodeIds.push(currentNodeId);

      const localNodeIds = incomingNodeIds.concat(
        outGoingNodeIds.filter(
          (item: any) => incomingNodeIds.indexOf(item) < 0,
        ),
      );
      if (localNodeIds.indexOf(currentNodeId) < 0) {
        localNodeIds.push(currentNodeId);
      }

      const localNodes = newNodes.filter((aNode: any) =>
        localNodeIds.includes(aNode.data.id),
      );
      let localEdges = newEdges
        .filter((edge: any) => localNodeIds.includes(edge.data.source))
        .filter((edge: any) => localNodeIds.includes(edge.data.target));

      // Filter self-reference edges
      localEdges = localEdges.filter(
        (edge: any) => edge.data.source !== edge.data.target,
      );

      // TODO: Find out why target ==='/' in some case
      localEdges = localEdges.filter((edge: any) => edge.data.target !== "/");
      return {
        nodes: localNodes,
        edges: localEdges,
      };
    } else {
      const filteredEdges = newEdges
        .filter((edge: any) => existingNodeIDs.includes(edge.data.source))
        .filter((edge: any) => existingNodeIDs.includes(edge.data.target));

      return {
        nodes: newNodes,
        edges: filteredEdges,
      };
    }
  }

  getAllSlugs(): Slug[] {
    const markdownFolder = Node.getMarkdownFolder();
    const markdownFiles = Node.getFiles(markdownFolder);
    const filePaths = markdownFiles.filter((file: string) => {
      if (file.endsWith("index")) return false;
      if (file.endsWith("sidebar")) return false;
      return true;
    });
    return filePaths.map((f: string) => {
      const sluggedFile = this.toSlug(f);
      return sluggedFile;
    });
  }

  /** Gets all directories */
  getDirectoryData(): RouteObj {
    const filteredDirectory = dirTree(Node.getMarkdownFolder(), {
      extensions: /\.md/,
      exclude: [/\.git/, /\.obsidian/],
    });
    const convertedDirectoryData = this.convertObject(filteredDirectory);
    this._directoryData = convertedDirectoryData;
    return this._directoryData;
  }

  convertObject(thisObject: RouteObj): RouteObj {
    const children: (typeof thisObject)[] = [];
    const slugs = this.getAllSlugs();

    function findFunc(_this: any, slug: string): boolean {
      const fileName = Transformer.parseFileNameFromPath(
        _this.toFilePath(slug),
      );
      return (
        Transformer.normalizeFileName(fileName) ===
        Transformer.normalizeFileName(thisObject.name)
      );
    }

    const foundSlugs = slugs.find((slug) => findFunc(this, slug));

    let routerPath = foundSlugs ? "/notes/" + foundSlugs : null;

    const newObject: RouteObj = {
      name: thisObject.name,
      children,
      id: (this._counter++).toString(),
      routePath: routerPath,
    };

    if (thisObject.children != null && thisObject.children.length > 0) {
      thisObject.children.forEach((aChild) => {
        const newChild = this.convertObject(aChild);
        children.push(newChild);
      });
    }

    return newObject;
  }

  flat = (routeList: RouteObj[]): Omit<RouteObj, "children">[] => {
    let result: Omit<RouteObj, "children">[] = [];
    const outerThis = this;
    routeList.forEach(function (a) {
      result.push(a);
      if (Array.isArray(a.children)) {
        result = result.concat(outerThis.flat(a.children));
      }
    });
    return result;
  };

  getFlattenArray(routeObj: RouteObj) {
    return this.flat(routeObj.children);
  }
}

const util = new Util();
export default util;
