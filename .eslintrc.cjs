module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'standard-with-typescript',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'next/core-web-vitals'
  ],
  overrides: [
    {
      env: { node: true },
      files: ['*.ts', '*.tsx'],
      parserOptions: {
        sourceType: 'script'
      }
    }
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: [
      './tsconfig.json'
    ]
  },
  plugins: [
    'react',
    '@typescript-eslint'
  ],
  rules: {
    'no-trailing-spaces': 'warn',
    'space-before-function-paren': 'off',
    '@typescript-eslint/space-before-blocks': 'off',
    'no-multi-spaces': 'warn',
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/space-infix-ops': 'warn',
    '@typescript-eslint/space-before-function-paren': 'off',
    '@typescript-eslint/explicit-function-return-type': 'warn',
    '@typescript-eslint/no-extraneous-class': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-unused-vars': 'warn',
    '@typescript-eslint/strict-boolean-expressions': 'off'
  },
  ignorePatterns: ['**/*.js*', 'src/**/*.test.ts', 'src/frontend/generated/*']
}
