import '../styles/global.css'
import '../styles/prism.css'
import type { AppProps } from 'next/app'
import type { HomeProps } from './notes'
import React from 'react'

export default function App({ Component, pageProps }: AppProps<HomeProps>): React.JSX.Element {
  const _Component: any = Component
  return (
    <_Component {...pageProps} />
  )
}
