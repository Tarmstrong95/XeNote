import {
  Html,
  Head,
  Main,
  NextScript,
} from "next/document";


export default function Document() {

  return (
    <Html>
      <Head>
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/gh/kimeiga/bahunya/dist/bahunya.min.css"
        />
      </Head>

      <body >
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
