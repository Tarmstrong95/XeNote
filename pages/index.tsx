import Head from 'next/head'
export default function Landing(): React.JSX.Element {
  return (
    <>
      <Head>
        <title>XeNote</title>
      </Head>

      <div className='w-full flex-col x-center' >
        <h1>Welcome to XeNote</h1>
        <a href="/notes" target="_blank" className='text-md font-muted font-bold '>🏃 Notes</a>
        <p>If you're seeing this, this is temporarily taking the place of my portfolio..</p>
        <p>It'll be back soon</p>
      </div >
    </>
  )
}



