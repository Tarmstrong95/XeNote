export function SocialLink({ SvgElement, link = "#" }) {
  return (
    <span>
      <a href={link} >
        {SvgElement}
      </a>
    </span >
  )
}
