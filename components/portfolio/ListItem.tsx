import ExternalLinkIcon from './ExternalLinkIcon'

export interface ListItemProps {
  label: string
  date?: string
  url: string
}

export default function ListItem({ label, date, url }: ListItemProps) {
  return (
    <div className="flex-row flex-between y-bottom gap-1">
      <Link {...{ url, label }} />
      <Divider />
      <Date {...{ date }} />
      <ExternalLinkIcon />
    </div>
  )
}

function Link({ url, label }: any) {
  return (
    <a href={url} target="_blank">
      {label}
    </a>
  )
}

function Divider() {
  return (
    <div style={{ border: '1px dashed #fff2' }} className="flex-1" />
  )
}

function Date({ date }: { date?: string }) {
  if (!date) return null
  return <i className="font-muted" >{date}</i>
}
