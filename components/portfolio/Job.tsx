export interface JobProps {
  logo: string
  name: string
  details: string
  dateFrom: string
  dateTo: string
}

export default function Job({ logo, name, details, dateFrom, dateTo }: JobProps) {
  return (
    <article className="flex gap-1" >
      <img src={logo} className="rounded lg bg-grey" />
      <span className="flex-1">
        <div className="flex flex-between">
          <span className="font-bold" > {name} </span>
          <span className="font-muted" > {dateFrom} - {dateTo} </span>
        </div>
        <span className="font-muted" > {details} </span>
      </span>
    </article>
  )
}
