export interface SkillProps {
  label: string
  link?: string
}

export default function Skill({ label, link = '#' }: SkillProps) {
  return (
    <a href={link} target="_blank" style={{ textDecoration: 'none !important' }}>
      {label}
    </a>
  )
}
