import ListItem from "../portfolio/ListItem"
import { UsefulLink } from "./UsefulLink"

export function UsefulLinksList() {
  return (
    <>
      <div className="flex-col">
        <h5>🧰 Tools</h5>
        <ListItem label="CSS-Tricks" url="" />
        <ListItem label="Indie Hackers" url="" />
        <ListItem label="W3Schools" url="" />
        <ListItem label="Simple Icons" url="https://simpleicons.org" />
        <ListItem label="Hero Icons" url="https://heroicons.com" />
        <ListItem label="Tailwind Cheatsheet" url="https://nerdcave.com/tailwind-cheat-sheet" />
        <ListItem label="Tailwind Elements" url="https://tw-elements.com" />
        <ListItem label="Tailwind Components" url="https://tailwindcomponents.com" />
      </div>

      <div className="flex-col">
        <h5>🎨 NoCss</h5>
        <ListItem label="Water CSS" url="https://watercss.kognise.dev" />
        <ListItem label="MPV CSS" url="https://andybrewer.github.io/mvp/" />
        <ListItem label="Bahunya" url="https://hakanalpay.com/bahunya/" />
        <ListItem label="Marx" url="https://mblode.github.io/marx/" />
        <ListItem label="Sakura" url="https://oxal.org/projects/sakura/" />
        <ListItem label="Tacit" url="https://yegor256.github.io/tacit/" />
        <ListItem label="New CSS" url="https://newcss.net" />
        <ListItem label="Bullframe CSS" url="https://marcop135.github.io/bullframe.css/" />
        <ListItem label="Markdown CSS (modest)" url="https://markdowncss.github.io/modest/" />
      </div>
    </>
  )
}
