import ExternalLinkIcon from "../portfolio/ExternalLinkIcon.js";

export function UsefulLink({ title, link }) {
  return (
    <a href={link} className="">
      <li className="">
        <div>{title}</div>
        <div className="" />
        <ExternalLinkIcon />
      </li>
    </a>
  )
}
