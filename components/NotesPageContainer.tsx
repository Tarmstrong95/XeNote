
export function NotesPageContainer({ children }: { children: React.ReactNode[] }) {
  return (
    <div className='flex gap-1 w-full' style={{ height: 'inherit' }}>
      {children}
    </div>
  )
}
